///E�refBurakKaya_152120171103
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

class Student {
public:
	vector<int>y;
	int score;
	void input() {
		int a;


		for (int i = 0; i < 5; i++) {
			cin >> a;
			y.push_back(a);
		}
	}
	int calculateTotalScore() {

		for (int i = 0; i < 5; i++) {
			score += y[i];
		}

		return score;
	}

};
void main() {
	int n; // number of students
	cin >> n;
	Student *s = new Student[n]; // an array of n students

	for (int i = 0; i < n; i++) {
		s[i].input();
	}

	// calculate kristen's score
	int kristen_score = s[0].calculateTotalScore();

	// determine how many students scored higher than kristen
	int count = 0;
	for (int i = 1; i < n; i++) {
		int total = s[i].calculateTotalScore();
		if (total > kristen_score) {
			count++;
		}
	}

	// print result
	cout << count;

	system("PAUSE");
}
