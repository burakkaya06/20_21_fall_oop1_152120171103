///E�refBurakKaya_152120171103
#include <iostream>
#include <sstream>
using namespace std;

class Student {
private:
	int inttemp1, inttemp2;
	string temp1, temp2;

public:
	void set_age(int x) {
		inttemp1 = x;
	}
	void set_standard(int y) {
		inttemp2 = y;
	}
	void set_first_name(string z) {
		temp1 = z;
	}
	void set_last_name(string t) {
		temp2 = t;
	}

	int get_age() {
		return inttemp1;
	}
	int get_standard() {
		return inttemp2;
	}
	string get_last_name() {
		return temp2;
	}
	string get_first_name() {
		return temp1;
	}

	string to_string() {
		stringstream ss;
		ss << inttemp1 << "," << temp1 << "," << temp2 << "," << inttemp2;
		string str = ss.str();
		return str;
	}
};

void main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	system("PAUSE");
}